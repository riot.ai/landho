import Vue from "vue"
const STORAGE_FILE = "landho.json";

export default (store) => {
  store.subscribe((mutation, state) => {
    if (mutation.type === "search/ADD_SEARCH") {
      gaia.save(state);
    }
  });

  store.subscribeAction({
    after: (action, state) => {
      if (action.type === "bulkImport") {
        gaia.save(state);
      }
    },
  });
};

const gaia = {
  save: (state) => {
    Vue.prototype.$userSession.putFile(STORAGE_FILE, JSON.stringify(state.search.userSearches));
  },
};