export const SET_SEARCHTERM = (state, term) => {
  state.currentSearchTerm = term;
};

export const ADD_SEARCH = (state, search) => {
  if (search.searchTerm !== "") {
    state.initialSearchComplete = true;
    state.currentSearch = search;
    state.currentSearchTerm = search.searchTerm;
    state.userSearches.unshift(state.currentSearch);
  }
};

export const INITIALIZE_SEARCH_DATA = (state, param) => {
  state.userSearches = param;
};

export const RM_SEARCH = (state, param) => {
  state.userSearches = state.userSearches.filter(
    search => search.timestamp !== param.timestamp
  );
};

export const CLEAR_HISTORY = state => {
  state.userSearches = [];
};

export const CLEAR_SEARCH = state => {
  state.currentSearch = {};
  state.currentSearchTerm = "";
};
