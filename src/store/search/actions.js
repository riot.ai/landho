// eslint-disable-next-line no-undef
const isEmpty = require("lodash.isempty");

export const addSearch = (context, params) => {
  if (!isEmpty(params)) {
    context.commit("ADD_SEARCH", params);
    context.commit("SET_SEARCHTERM", params.searchTerm);
  }
};
export const initializeFromGaia = (context, params) => {
  context.commit("INITIALIZE_SEARCH_DATA", params);
};
export const removeSearch = (context, params) => {
  context.commit("RM_SEARCH", params);
};

export const clearHistory = context => {
  context.commit("CLEAR_HISTORY");
};

export const clearSearch = context => {
  context.commit("CLEAR_SEARCH");
};

export const setSearchTerm = (context, param) => {
  context.commit("SET_SEARCHTERM", param);
};
