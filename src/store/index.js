import Vue from "vue";
import Vuex from "vuex";
import gaiaplugin from "./gaiaplugin"

// import modules
import search from "./search";
import user from "./user";

Vue.use(Vuex);

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    plugins: [gaiaplugin],
    modules: {
      // then we reference it
      search,
      user
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  /*
    if we want some HMR magic for it, we handle
    the hot update like below. Notice we guard this
    code with "process.env.DEV" -- so this doesn't
    get into our production build (and it shouldn't).
  */

  // if (process.env.DEV && module.hot) {
  //   module.hot.accept(["./search"], () => {
  //     const newSearch = require("./search").default;
  //     Store.hotUpdate({ modules: { search: newSearch } });
  //   });
  // }

  return Store;
}
