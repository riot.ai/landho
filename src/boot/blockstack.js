import Vue from "vue"
import { UserSession, AppConfig } from "blockstack";

export const appConfig = new AppConfig(['store_write'])
export const userSession = new UserSession({ appConfig })

export default ({ router, Vue }) => {

  // trying to use router per example at
  // https://quasar.dev/quasar-cli/cli-documentation/boot-files#Router-authentication
  //
  router.beforeEach((to, from, next) => {
    if (to.matched.some(page => page.meta.requiresAuth)) {
      if (!userSession.isUserSignedIn()) {
        next("/");
      } else {
        next();
      }
    } else {
      next();
    }
  });
};

Vue.prototype.$userSession = userSession;
