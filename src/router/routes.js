import LandingLayout from "../layouts/LandingLayout";
import AppLayout from "../layouts/AppLayout";
import Search from "pages/Search";
import History from "pages/History";
import Index from "pages/Index"; // landing page

const routes = [
  {
    path: "/",
    component: LandingLayout,
    children: [
      {
        path: "",
        component: Index,
        // component: () => import("pages/Index.vue")
      },
    ],
  },
  {
    path: "/app",
    component: AppLayout,
    // component: () => import("layouts/app.vue"),
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "search",
        component: Search,
        // component: () => import("pages/search.vue"),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "history",
        component: History,
        // component: () => import("pages/history.vue"),
        meta: {
          requiresAuth: true,
        },
      },
    ],
  },
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

// Always leave this as last one
// if (process.env.MODE !== "ssr") {
//   routes.push({
//     path: "*",
//     component: () => import("pages/Error404.vue")
//   });
// }

export default routes;
